const settingModel = require("../model/setting");
const multer = require("multer");
const path = require("path");
const fs = require("fs");

var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, path.join(__dirname, "../uploads/logo"));
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + path.extname(file.originalname));
    },
});
var upload = multer({ storage: storage });

const createLogo = async(req, res) => {
    let newLogo = req.body;
    const img = req.file;
    console.log(img);
    newLogo.image = img.filename;
    const insert = new settingModel(newLogo);
    await insert.save(async(err, result) => {
        if (err) console.log(err);
        if (result._id) {
            const logos = await settingModel.find({});
            res
                .json({
                    status: 201,
                    message: "บันทึกข้อมูโลโก้เรียบร้อย",
                    abouts: logos,
                })
                .end();
        } else {
            res
                .json({
                    status: 200,
                    message: "บันทึกข้อมูลโลโก้ล้มเหลว กรุณาลองใหม่อีกครั้ง",
                })
                .end();
        }
    });
};

const getLogo = async(req, res) => {
    try {
        await settingModel.find({}, (err, result) => {
            if (err) {
                res.send(401);
            }
            let photoObject = "";
            result.length > 0 ? (photoObject = result[0]) : "";
            res.json({ status: 200, photo: photoObject });
        });
    } catch (error) {
        res.send(401);
    }
};

module.exports = {
    createLogo,
    getLogo,
    upload,
};