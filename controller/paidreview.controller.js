const paidreviewModel = require("../model/paidreview");
const orderModel = require("../model/order");
const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc");
const tz = require("dayjs/plugin/timezone");
dayjs.extend(utc);
dayjs.extend(tz);
dayjs.tz.setDefault("Asia/Bangkok");

const paidreview = async(req, res) => {
    const { id } = req.params;
    const resultOrder = await orderModel.findOne({ _id: id });
    if (resultOrder) {
        let confirmOrder = 1;
        let statusPaid = 0;
        const {
            customerId,
            bankId,
            note,
            total,
            discount,
            totalprice,
            code,
            shippingAddress,
            products,
            orderStatus,
            slip,
        } = resultOrder;
        const payload = {
            customerId,
            bankId,
            note,
            total,
            discount,
            totalprice,
            code,
            shippingAddress,
            products,
            orderStatus,
            slip,
            paidStatus: statusPaid,
            confirmStatus: confirmOrder,
        };
        const newCreated = new paidreviewModel(payload);
        await newCreated.save(async(err, resultInsert) => {
            if (err) {
                res.json({
                    status: 401,
                    orders: [resultInsert],
                    message: "แก้ไขสถานะการจัดส่งล้มเหลว",
                });
            }
            if (resultInsert) {
                await orderModel.findOneAndDelete({ _id: id });
                //insert Items
                res.json({
                    status: 200,
                    orders: [resultInsert],
                    message: "แก้ไขสถานะการจัดส่งเรียบร้อย รายการนี้จะถูกย้ายไปประวัติการซื้อขาย",
                });
            } else {
                res.json({
                    status: 401,
                    orders: [resultInsert],
                    message: "แก้ไขสถานะการจัดส่งล้มเหลว",
                });
            }
        });
    } else {
        const resultOrder = await orderModel.find({ _id: id });
        res.json({
            status: 401,
            orders: resultOrder,
            message: "แก้ไขสถานะการจัดส่งล้มเหลว",
        });
    }
};


//---------------------------Get order from paidreviewDB----------
const getpaid = async(req, res) => {
    let customerId = req.params.customerId;
    console.log(customerId, "customerIds");
    try {
        const orders = await paidreviewModel.find({ customerId: customerId },
            async function(err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Result : ", "Paidreview");
                }
            }
        );
        console.log(orders);
        await res.json({
            status: 200,
            message: "มีข้อมูลออร์เดอร์",
            orders: orders,
        });
    } catch (error) {
        console.log(error);
    }
};
//----------------------Update Paidorder Status---------------------------------------//
const updatedPaidOderStatus = async(req, res) => {
    try {
        const { id } = req.params;
        const { comment, statusPaidsOder } = req.body;
        await paidreviewModel.findByIdAndUpdate(
            id, {
                $set: { comment: comment, orderStatus: statusPaidsOder },
            },
            async function(error, response) {
                if (error) {
                    res.json({ status: 400, message: "แก้ไขข้อมูลล้มเหลว" });
                }
                if (response) {
                    const result = await paidreviewModel
                        .find({ _id: id })
                    res.json({
                        status: 200,
                        message: "บันทึกข้อมูลสำเร็จ",
                        history: result,
                    });
                } else {
                    res.json({
                        status: 400,
                        message: "บันทึกข้อมูลล้มเหลว กรุณาลองใหม่อีกครั้ง",
                    });
                }
            }
        );
    } catch (error) {
        res.json({ status: 400, message: "แก้ไขข้อมูลล้มเหลว" });
    }
};




module.exports = {
    paidreview,
    getpaid,
    updatedPaidOderStatus
};