const express = require("express");
const route = express.Router();
const { verifyToken } = require("../middlewares/authJwtAdmin");

const {
    getOrderCmsAdmin,
    getOrderCmsAdminwithId,
    updatedPendingStatus,
    getOrder,
    updateCancelOrder,
    createOrder,
    getOrderAddress,
    updateAddressOrder,
    uploadSlip,
    upload,
    updateSlip
} = require("../controller/order");

const { getpaid, updatedPaidOderStatus } = require('../controller/paidreview.controller')


route.post("/order", createOrder);
route.put("/order/cancel/:id", updateCancelOrder);
route.get("/order/:customerId", getOrder);
route.get("/order/getAddress/:id", getOrderAddress);
route.put("/order/changeAddress/:id/:shippingId", updateAddressOrder);
route.post("/order/uploadSlip/:id", upload.single("photo"), uploadSlip);
//Update Slip
route.put("/order/updateSlip/:id", upload.single("photo"), updateSlip);

//Orderstatus Update
route.get('/order/paidreview/:customerId', getpaid)


/* route admin [verifyToken] */
route.get("/cms/order", getOrderCmsAdmin);
route.get("/cms/order/:id", getOrderCmsAdminwithId);
route.put("/cms/order/paid/:id", updatedPendingStatus);

//----update paidOrderStatus-------------//
route.put("/cms/order/paidorder/:id", updatedPaidOderStatus);

module.exports = route;