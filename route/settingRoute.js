const express = require("express");
const route = express.Router();

const setting = require("../controller/setting");

route.get("/setting", setting.getLogo);
// route.post('/setting', setting.upload.single("photo"), setting.createLogo);

route.post('/setting', setting.upload.single("photo"), setting.createLogo);


module.exports = route;