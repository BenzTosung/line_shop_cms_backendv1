const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const settingSchema = Schema({
    company: String,
    photo: String,
}, { timestamps: true, versionKey: false });

const settingModel = mongoose.model("setting", settingSchema);

module.exports = settingModel;